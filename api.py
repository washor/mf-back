import flask
from flask import request
from flask import send_from_directory
import psycopg2
import http.client
import requests
import sys
import os

app = flask.Flask(__name__)
app.config["DEBUG"] = True

# Test endpoint to check database connectivity
@app.route('/', methods=['GET'])
def home():
    conn = makeConn()

    # create a cursor
    cur = conn.cursor()
        
	# execute a statement
    print('PostgreSQL database version:')
    cur.execute('SELECT version()')

    # display the PostgreSQL database server version    
    db_version = cur.fetchone()

    cur.close()
    conn.close()

    return "<h1>DB Version</h1><p>"+ str(db_version) +"</p>"

# Endpoint to return QR images
@app.route('/img/', methods=['GET'])
def getQRImage():
    # Get inputs
    uid = request.args.get('userid')
    sn = request.args.get('productsn')

    root_dir = os.path.dirname('C:\\src\\markforged\\back-end\\files\\')
    filename = str(uid) + "_._" + str(sn) + '.png'

    return send_from_directory(root_dir, filename)

# Endpoint to create or update the QR image in the database and filesystem
@app.route('/add/', methods=['GET'])
def add():
    # Get our inputs
    uid = request.args.get('userid')
    sn = request.args.get('productsn')
    url = request.args.get('url')

    # Generate QR Code and Return Success or Failure
    response = generateQRCode(url)
    imgFile = saveQRCode(uid, sn, response)
    if not imgFile:
        return '{"success": false}'

    # Make DB Connection Object and Update Saved Info
    conn = makeConn()
    result = crupdateQR(conn, uid, sn, url, imgFile)
    conn.close()

    return result

# Function to create QR code from external QR Code Monkey API
def generateQRCode(url):
    apiUrl = "https://qrcode-monkey.p.rapidapi.com/qr/custom"

    querystring = {"data":url,"size":"600","file":"png","config":"{\"bodyColor\": \"#16c0b0\", \"bgColor\": \"#FFFFFF\", \"eye\": \"frame14\", \"eyeBall\": \"ball1\", \"body\":\"edge-cut-smooth\"}"}

    headers = {
        'x-rapidapi-key': '9b3788b341msh9bff5c543747af2p16fdbbjsn7c7322ab8e9e',
        'x-rapidapi-host': 'qrcode-monkey.p.rapidapi.com'
        }

    response = requests.request("GET", apiUrl, headers=headers, params=querystring)

    return response

# Saves the QR code image file into the local filesystem
def saveQRCode(uid, sn, response):
    if response.status_code == 200:
        filename = 'files\\' + str(uid) + "_._" + str(sn) + ".png"

        with open(filename, 'wb') as f:
            for chunk in response:
                f.write(chunk)
            f.close()
    else:
        return False
    return filename

# Creates and updates QR code information in the database
def crupdateQR(conn, uid, sn, url, img):
    # Build return object
    result = '{"userid": ' + uid + ', "productsn": \"' + sn + '\", "url": \"' + url + '\", "image": \"' + img + '\"}'

    cur = conn.cursor()
    try:
        cur.execute("""
            INSERT INTO qr (userid, productsn, url, image)
            VALUES (%(uid)s, %(sn)s, %(url)s, %(img)s);
            """,
            {'uid': uid, 'sn': sn, 'url': url, 'img': img})
    except:
        # Capture error for debugging purposes
        e = sys.exc_info()[0]
        # Close out failed connection
        cur.close()
        conn.close()

        # Try updating
        conn = makeConn()
        cur = conn.cursor()
        try:
            cur.execute("""
                UPDATE qr SET url = %(url)s, image = %(img)s WHERE userid = %(uid)s AND productsn = %(sn)s
                """,
                {'url': url, 'img': img, 'uid': uid, 'sn': sn})
        except:
            e = sys.exc_info()[0]
            print(e)
    conn.commit()
    cur.close()

    return result

# Makes the DB connection - don't forget to close it when you are done!
def makeConn():
    conn = psycopg2.connect(
        host="localhost",
        database="markforged",
        user="postgres",
        password="Z5v0vC04HYxfcmoA"
    )
    return conn

# Flask API main invocation
app.run()
